var express = require("express");
var app = express();
let bodyParser = require("body-parser");
let http = require("http");
let fileType = require("file-type");

let bot_orders = [];

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

function existsInArray(element, array) {
  for (let i = 0; i < array.length; i++) {
    if (element === array[i]) return true;
  }
  return false;
}


app.post("/removeduplicatewords", function(req, res) {
  let palabras = req.body.palabras.split(",");
  let arr = [];
  let result = "";
  for (let i = 0; i < palabras.length; i++) {
    if (!existsInArray(palabras[i], arr)) {
      arr.push(palabras[i]);
      if (arr.length > 1) {result += ","}
      result += palabras[i];
    }
  }
  res.end(result);
});


app.post("/detectfiletype", function(req, res) {
  let url = req.body.url;
  let type_result;
  http.get(url, response => {
    response.once('data', chunk => {
        response.destroy();
        type_result = fileType(chunk);
        console.log(type_result);
        res.end(JSON.stringify(type_result));
    });
  });
});


app.get("/botorder/:order", function(req, res) {
  let order = req.params.order;
  if (!bot_orders[order]) {
    res.end("NONE");
  } else {
    res.end(bot_orders[order]);
  }
});

app.post("/botorder/:order", function(req, res) {
  let order = req.params.order;
  bot_orders[order] = req.body.botorder;
  res.end("OK");
});






app.listen(8000);
